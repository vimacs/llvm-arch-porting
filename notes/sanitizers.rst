sanitizers 的支持
=========================

要对某个目标启用一个 sanitizer, 首先要在 Clang driver 中启用。具体可见 Clang 源码中 lib/Driver/ToolChain.cpp 和 lib/Driver/ToolChains/ 目录下各目标对应的源文件，有 getSupportedSanitizers() 函数。
