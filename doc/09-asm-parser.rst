AsmParser
================

我们现在来实现 llvm-mc 的汇编器。执行 ``bin/llvm-mc -arch=rv32i --assemble`` 之后按 Ctrl-D 结束输入，得到错误::

  bin/llvm-mc: error: this target does not support assembly parsing.

查 LLVM 的代码，发现它来自 llvm-mc::

  std::unique_ptr<MCAsmParser> Parser(
      createMCAsmParser(SrcMgr, Ctx, Str, MAI));
  std::unique_ptr<MCTargetAsmParser> TAP(
      TheTarget->createMCAsmParser(STI, *Parser, MCII, MCOptions));

  if (!TAP) {
    WithColor::error(errs(), ProgName)
        << "this target does not support assembly parsing.\n";
    return 1;
  }

和之前介绍过的组件一样， ``Target::createMCAsmParser`` 接口调用在 Target 代码中注册的 AsmParser 构造函数。按照以前的做法，我们可以在 MCTargetDesc/RV32IMCTargetDesc.cpp 的 ``LLVMInitializeRV32ITargetMC()`` 中加上::

  TargetRegistry::RegisterMCAsmParser(T, createRV32IMCAsmParser);

并实现此函数::

  static MCTargetAsmParser *createRV32IMCAsmParser(
    const MCSubtargetInfo &STI, MCAsmParser &P, const MCInstrInfo &MII,
    const MCTargetOptions &Options);

但是 LLVM 对 AsmParser 这个模块的注册约定的用法不太一样。实际的用法和我们在 `Disassembler <07-disassembler.rst>`__ 中见过的用法一样。我们还是看 RISC-V 的代码，AsmParser 在 AsmParser/RISCVAsmParser.cpp 中实现，在这个文件的末尾加上注册 AsmParser 的函数::

  extern "C" LLVM_EXTERNAL_VISIBILITY void LLVMInitializeRISCVAsmParser() {
    RegisterMCAsmParser<RISCVAsmParser> X(getTheRISCV32Target());
    RegisterMCAsmParser<RISCVAsmParser> Y(getTheRISCV64Target());
  }

在 llvm/MC/TargetRegistry.h 中，RegisterMCAsmParser 是一个类模板，在实例化的时候为一个 Target 注册 AsmParser::

  template <class MCAsmParserImpl> struct RegisterMCAsmParser {
    RegisterMCAsmParser(Target &T) {
      TargetRegistry::RegisterMCAsmParser(T, &Allocator);
    }

  private:
    static MCTargetAsmParser *Allocator(const MCSubtargetInfo &STI,
                                        MCAsmParser &P, const MCInstrInfo &MII,
                                        const MCTargetOptions &Options) {
      return new MCAsmParserImpl(STI, P, MII, Options);
    }
  };

我们仿照 RISC-V 的来，加上 AsmParser 目录，修改 RV32I/CMakeLists.txt，
仿照 RISCV 创建 RV32I/AsmParser/CMakeLists.txt，写一个空的
RV32IAsmParser 实现::

  #include "llvm/MC/TargetRegistry.h"
  #include "llvm/MC/MCParser/MCTargetAsmParser.h"
  #include "TargetInfo/RV32ITargetInfo.h"
  
  using namespace llvm;
  
  class RV32IAsmParser : public MCTargetAsmParser {
  public:
    RV32IAsmParser(const MCSubtargetInfo &STI, MCAsmParser &P,
                   const MCInstrInfo &MII, const MCTargetOptions &Options):
        MCTargetAsmParser(Options, STI, MII)
    {
    }
  };
  
  extern "C" LLVM_EXTERNAL_VISIBILITY void LLVMInitializeRV32IAsmParser() {
    RegisterMCAsmParser<RV32IAsmParser> X(getTheRV32ITarget());
  }

编译出错，原因是 MCTargetAsmParser 是个虚基类，有些虚函数必须实现。我们在 include/llvm/MC/MCParser/MCTargetAsmParser.h 可以找到这些纯虚函数::

  virtual bool ParseRegister(unsigned &RegNo, SMLoc &StartLoc,
                             SMLoc &EndLoc) = 0;

  virtual OperandMatchResultTy
  tryParseRegister(unsigned &RegNo, SMLoc &StartLoc, SMLoc &EndLoc) = 0;

  virtual bool ParseInstruction(ParseInstructionInfo &Info, StringRef Name,
                                SMLoc NameLoc, OperandVector &Operands) = 0;

  virtual bool ParseDirective(AsmToken DirectiveID) = 0;

  virtual bool MatchAndEmitInstruction(SMLoc IDLoc, unsigned &Opcode,
                                       OperandVector &Operands, MCStreamer &Out,
                                       uint64_t &ErrorInfo,
                                       bool MatchingInlineAsm) = 0;

  virtual void convertToMapAndConstraints(unsigned Kind,
                                          const OperandVector &Operands) = 0;

但不是所有的这些虚函数都要手动实现，有的是由 TableGen 生成的。对 RV32I.td 使用 TableGen 生成 AsmMatcher 的代码::

  /build-tblgen/bin/llvm-tblgen -gen-asm-matcher -Illvm/include -Illvm/lib/Target/RV32I llvm/lib/Target/RV32I/RV32I.td

可以看到 ``convertToMapAndConstraints`` 这个函数是自动生成的，其他的还是需要自己实现。

首先我们把自动生成的函数的函数声明放到 RV32IAsmParser 类中::

  #define GET_ASSEMBLER_HEADER
  #include "RV32IGenAsmMatcher.inc"

之后逐个实现上述虚接口，其中 ParseRegister 和 tryParseRegister 基于 RISCV 的再简化一下，主要工作包括用 TableGen 生成的 ``MatchRegisterName`` 函数把寄存器名转为用枚举值表示的 LLVM 寄存器号。

接着实现 ``ParseInstruction``. 这个过程的4个参数中，我们重点看 Name 和 Operands 两个参数。Name 通常是操作码的名字。Operands 是个输出参数，需要解析了整个汇编指令后将每个操作数存入其中，其中操作码的助记符被当作第一个操作数。

可以看出，在 AsmParser 中，我们需要先实现 Operands.

