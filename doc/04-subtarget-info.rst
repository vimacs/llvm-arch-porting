SubtargetInfo
================

接下来我们实现 SubTargetInfo. 先看 RISCV 的 MCTargetDesc/RISCVMCTargetDesc.cpp::

  static MCSubtargetInfo *createRISCVMCSubtargetInfo(const Triple &TT,
                                                     StringRef CPU, StringRef FS) {
    if (CPU.empty())
      CPU = TT.isArch64Bit() ? "generic-rv64" : "generic-rv32";
    if (CPU == "generic")
      report_fatal_error(Twine("CPU 'generic' is not supported. Use ") +
                         (TT.isArch64Bit() ? "generic-rv64" : "generic-rv32"));
    return createRISCVMCSubtargetInfoImpl(TT, CPU, /*TuneCPU*/ CPU, FS);
  }

注意 ``createRISCVMCSubtargetInfoImpl`` 这个函数，它不在 RISCV 的代码
中，可以很容易地想到，这是 TableGen 生成出来的函数。通过搜索可以发现它
在 RISCVGenSubtargetInfo.inc 里面，通过 TableGen 的 ``-gen-subtarget``
参数生成。

先看一下这个生成的文件，可以看到它记录了 RISCV 的 Subtarget 的多种特性
(SubtargetFeatures)，包括这个体系结构支持的多种指令扩展。其中64位支持，
即 RV64，也属于一个 RISCV Subtarget feature.

我们对 RV32I 运行一下 ``llvm-tblgen -gen-subtarget``, 可以发现
TableGen 生成了代码，其中包含了 ``createRV32IMCSubtargetInfoImpl`` 函
数。也就是说，我们不用编写关于 RV32I 的描述代码，TableGen 也能生成关于
它的 SubtargetInfo 代码。

我们在 CMakeLists.txt 中加上一条 TableGen 命令生成 SubtargetInfo 的代
码，在 MCTargetDesc/RV32IMCTargetDesc.cpp 加上::

  #include <llvm/MC/MCSubtargetInfo.h>
  /* ... */
  #define GET_SUBTARGETINFO_MC_DESC
  #include "RV32IGenSubtargetInfo.inc"
  /* ... */
  static MCSubtargetInfo *createRV32IMCSubtargetInfo(const Triple &TT,
                                                     StringRef CPU, StringRef FS) {
    return createRV32IMCSubtargetInfoImpl(TT, CPU, /*TuneCPU*/ CPU, FS);
  }

之后编译出错，提示 ``extern const llvm::SubtargetSubTypeKV RV32ISubTypeKV[]`` 数组大小为0.
因此，我们还是需要加上 Subtarget 的描述。

从 RISCV 的 RISCVSubTypeKV 这个数组里的 ``generic-rv32``,
``sifive-7-rv32`` 等字符串，可以找到这个数组的内容来自于 RISCV.td 的如
下定义::

  def : ProcessorModel<"generic-rv32", NoSchedModel, []>;
  def : ProcessorModel<"generic-rv64", NoSchedModel, [Feature64Bit]>;
  
  def : ProcessorModel<"rocket-rv32", RocketModel, []>;
  def : ProcessorModel<"rocket-rv64", RocketModel, [Feature64Bit]>;
  
  def : ProcessorModel<"sifive-7-rv32", SiFive7Model, []>;
  def : ProcessorModel<"sifive-7-rv64", SiFive7Model, [Feature64Bit]>;
  
  def : ProcessorModel<"sifive-e31", RocketModel, [FeatureStdExtM,
                                                   FeatureStdExtA,
                                                   FeatureStdExtC]>;

其中 ProcessorModel 来自 include/llvm/Target/Target.td::

  class ProcessorModel<string n, SchedMachineModel m, list<SubtargetFeature> f,
                       list<SubtargetFeature> tunef = []>
    : Processor<n, NoItineraries, f, tunef> {
    let SchedModel = m;
  }

此外，RISCV.td 定义这些 ProcessorModel 用的是 ``def : ...``, 并没有指
定所定义实体的名字。用 llvm-tblgen 直接生成记录，可以发现这些定义产生
了一堆名为 ``anonymous_...`` 的记录，也就是说当我们不需要为这些定义命
名时，可以用这种写法产生匿名记录。

搞清楚这些之后，我们在 RV32I.td 中加上一行::

  def : ProcessorModel<"generic-rv32i", NoSchedModel, []>;

接着就能通过编译了。现在运行 llvm-mc 使用反汇编器，看到的错误是无法创
建 instruction info, 这是我们接下来的工作。
