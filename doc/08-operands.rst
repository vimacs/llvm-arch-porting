指令的操作数
===============

之前我们在 LLVM 中实现了一个 Nop 指令，这个指令没有任何操作数。而在实际的处理器中，大多数指令都是有操作数的。我们现在先尝试实现一个有操作数的指令。

我们先按照 RISC-V 的文档，定义 R 型指令，它接受 3 个寄存器操作数：rd 为目的操作数，rs1 和 rs2 为源操作数::

  class RV32I_RType_Inst<bits<7> funct7, bits<3> funct3, bits<7> opc,
                         dag outs, dag ins, string opcodestr, string argstr>
      : RV32IInst<outs , ins, opcodestr, argstr> {
    bits<5> rd;
    bits<5> rs1;
    bits<5> rs2;
  
    let Inst{11-7} = rd;
    let Inst{19-15} = rs1;
    let Inst{24-20} = rs2;
  
    let Inst{14-12} = funct3;
    let Inst{31-25} = funct7;
  
    let Opcode = opc;
  }

我们删掉之前的 Nop 指令，添加以下 RISC-V 的寄存器加法指令::

  def Add_rr: RV32I_RType_Inst<0b0000000, 0b000, 0b0110011,
                               (outs GPR:$rd), (ins GPR:$rs1, GPR:$rs2),
                               "add", "$rd, $rs1, $rs2">;

这里 GPR 是之前在 RV32IRegisterInfo.td 中定义的 RegisterClass. ``GPR:$rd`` 将指令里的 rd 字段指定为 GPR 类型。

在这里，我们顺便把 X0, X1, ..., X31 这 32 个寄存器全部定义并加到 GPR 中。

现在我们构建 llvm-mc, 出现错误::

  In file included from .../llvm/lib/Target/RV32I/MCTargetDesc/RV32IInstPrinter.cpp:8:
  .../build-myrv32i/lib/Target/RV32I/RV32IGenAsmWriter.inc: 在成员函数‘void llvm::RV32IInstPrinter::printInstruction(const llvm::MCInst*, uint64_t, llvm::raw_ostream&)’中:
  .../build-myrv32i/lib/Target/RV32I/RV32IGenAsmWriter.inc:305:5: 错误：‘printOperand’在此作用域中尚未声明
  305 |     printOperand(MI, 0, O);
      |     ^~~~~~~~~~~~

  .../llvm/lib/Target/RV32I/Disassembler/RV32IDisassembler.cpp:53:42:   required from here
  .../build-myrv32i/lib/Target/RV32I/RV32IGenDisassemblerTables.inc:91:31: 错误：‘DecodeGPRRegisterClass’在此作用域中尚未声明
   91 |     if (DecodeGPRRegisterClass(MI, tmp, Address, Decoder) == MCDisassembler::Fail) { return MCDisassembler::Fail; }

也就是说，我们要加上打印和解码操作数的相关函数。

打开生成的 RV32IGenAsmWriter.inc, 可以看到::

  // Add_rr
  printOperand(MI, 0, O);
  O << ", ";
  printOperand(MI, 1, O);
  O << ", ";
  printOperand(MI, 2, O);

这样的代码，功能是逐个打印 ``MCInst *MI`` 的操作数到 ``raw_ostream &O``.

我们仿照 RISCVInstPrinter 定义 ``RV32IInstPrinter::printOperand``::

  void RV32IInstPrinter::printOperand(const MCInst *MI, unsigned OpNo, raw_ostream &O)
  {
    const MCOperand &MO = MI->getOperand(OpNo);
  
    if (MO.isReg()) {
      O << getRegisterName(MO.getReg());
      return;
    }
  
    if (MO.isImm()) {
      O << MO.getImm();
      return;
    }
  
    assert(false && "Unknown operand kind in printOperand");
  }

其中 getRegisterName 函数是 TableGen 生成的，作用是把寄存器号转换为寄存器名。

接着实现 DecodeGPRRegisterClass. RV32IGenDisassemblerTables.inc 是这样用的::

  tmp = fieldFromInstruction(insn, 7, 5);
  if (DecodeGPRRegisterClass(MI, tmp, Address, Decoder) == MCDisassembler::Fail) { return MCDisassembler::Fail; }
  tmp = fieldFromInstruction(insn, 15, 5);
  if (DecodeGPRRegisterClass(MI, tmp, Address, Decoder) == MCDisassembler::Fail) { return MCDisassembler::Fail; }
  tmp = fieldFromInstruction(insn, 20, 5);
  if (DecodeGPRRegisterClass(MI, tmp, Address, Decoder) == MCDisassembler::Fail) { return MCDisassembler::Fail; }

就是把指令中的几段比特拿出来，让 DecodeGPRRegisterClass 译码。

我们仿照 RISC-V 的做法，在 RV32IDisassembler.cpp 中加上这个函数::

  static DecodeStatus DecodeGPRRegisterClass(MCInst &Inst, uint64_t RegNo,
                                             uint64_t Address,
                                             const void *Decoder) {
    if (RegNo >= 32)
      return MCDisassembler::Fail;
  
    MCRegister Reg = RV32I::X0 + RegNo;
    Inst.addOperand(MCOperand::createReg(Reg));
    return MCDisassembler::Success;
  }

由于需要用到 ``RV32I::X0``, 我们创建 MCTargetDesc/RV32IMCTargetDesc.h 并把 RV32IMCTargetDesc.cpp 里的这两行移过去::

  #define GET_REGINFO_ENUM
  #include "RV32IGenRegisterInfo.inc"

现在编译 llvm-mc 之后运行::

  $ bin/llvm-mc -arch=rv32i -disassemble
  0xb3 0x80 0x00 0x00 
  0xb3 0x00 0x00 0x00
  0xb3 0x80 0x10 0x00
          .text
          add X1, X1, X0
          add X1, X0, X0
          add X1, X1, X1

这和 RISC-V 反汇编器的结果一致。
