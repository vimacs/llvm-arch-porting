AsmInfo
==========

接上一节，我们需要一个AsmInfo. LLVM 的 MCAsmInfo 类在
llvm/MC/MCAsmInfo.h 头文件中声明，其成员主要是关于汇编和反汇编的属性，
少部分和机器相关。

我们直接从 MCAsmInfo 继承一个目标相关的类即可。RISCVMCAsmInfo 继承的是
MCAsmInfoELF, 我们也继承这个类，实现暂时全部留空::

  // in MCTargetDesc/RV32IMCAsmInfo.h
  class RV32IMCAsmInfo: public MCAsmInfoELF {
  public:
    explicit RV32IMCAsmInfo(const Triple &TargetTriple);
  };

  // in MCTargetDesc/RV32IMCAsmInfo.cpp
  RV32IMCAsmInfo::RV32IMCAsmInfo(const Triple &TT) {
  }

  // in MCTargetDesc/RV32IMCTargetDesc.cpp
  static MCAsmInfo *createRV32IMCAsmInfo(const MCRegisterInfo &MRI,
                                         const Triple &TT,
                                         const MCTargetOptions &Options) {
    MCAsmInfo *MAI = new RV32IMCAsmInfo(TT);
    return MAI;
  }

在 MCTargetDesc/CMakeLists.txt 加上 RV32IMCAsmInfo.cpp, 即可编译通过。
运行 llvm-mc 可以发现不再出现找不到 MCAsmInfo 的错误，错误是找不到
subtarget info, 我们接下来实现这个。
