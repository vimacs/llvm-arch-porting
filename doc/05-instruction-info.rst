InstructionInfo
======================

现在我们开始添加 instruction info. 首先还是用 TableGen 的 ``--gen-instr-info`` 来看看能不能生成它，得到的是错误信息::

  llvm/lib/Target/RV32I/RV32I.td:4:1: error: Record `RV32I', field `InstructionSet' does not have a def initializer!
  def RV32I : Target {

直接输出 RV32I.td 对应的记录，看到的是::

  class Target {
    InstrInfo InstructionSet = ?;
    list<AsmParser> AssemblyParsers = [DefaultAsmParser];
    list<AsmParserVariant> AssemblyParserVariants = [DefaultAsmParserVariant];
    list<AsmWriter> AssemblyWriters = [DefaultAsmWriter];
    int AllowRegisterRenaming = 0;
  }

  def RV32I {     // Target
    InstrInfo InstructionSet = ?;
    list<AsmParser> AssemblyParsers = [DefaultAsmParser];
    list<AsmParserVariant> AssemblyParserVariants = [DefaultAsmParserVariant];
    list<AsmWriter> AssemblyWriters = [DefaultAsmWriter];
    int AllowRegisterRenaming = 0;
  }

就是说 Target 类有一个类型为 ``InstrInfo`` 的 ``InstructionSet`` 字段，我们没有设置，所以 TableGen 不能生成 instruction info.

而输出 RISCV 的记录，就可以看到::

  def RISCV {     // Target
    InstrInfo InstructionSet = RISCVInstrInfo;
    list<AsmParser> AssemblyParsers = [RISCVAsmParser];
    list<AsmParserVariant> AssemblyParserVariants = [DefaultAsmParserVariant];
    list<AsmWriter> AssemblyWriters = [RISCVAsmWriter];
    int AllowRegisterRenaming = 1;
  }

再看 RISCV.td 可以看到::

  def RISCVInstrInfo : InstrInfo {
    let guessInstructionProperties = 0;
  }

  def RISCV : Target {
    let InstructionSet = RISCVInstrInfo;
    let AssemblyParsers = [RISCVAsmParser];
    let AssemblyWriters = [RISCVAsmWriter];
    let AllowRegisterRenaming = 1;
  }

我们在 RV32I 里面也这样做::

  def RV32IInstrInfo : InstrInfo {
  }
  
  def RV32I : Target {
    let InstructionSet = RV32IInstrInfo;
    //let AssemblyParsers = [RISCVAsmParser];
    //let AssemblyWriters = [RISCVAsmWriter];
    //let AllowRegisterRenaming = 1;
  }

这次再跑一次 TableGen, 错误提示为 ``error: No instructions defined!``, 就是说我们还没有定义指令。

查看 LLVM 源码的 ``llvm/Target/Target.td``, 可以找到一个 ``class Instruction`` 定义。
我们模仿其他体系结构的做法，单独建一个 ``RV32IInstrInfo.td`` 文件并让 ``RV32I.td`` 包含它，然后在里面加上::

  def Nop : Instruction {
    dag OutOperandList = (outs);
    dag InOperandList = (ins);
  }

``OutOperandList`` 和 ``InOperandList`` 这两个字段来自 ``Instruction`` 基类，是必须设置的，否则 TableGen 会报它们没有定义。这时候再运行 TableGen, 还是报 ``No instructions defined!`` 的错误。为了明白为什么会发生这个错误，我们看 TableGen 生成 Instruction Info 相关的源码::

  // utils/TableGen/InstrInfoEmitter.cpp

  const CodeGenTarget &Target = CDP.getTargetInfo();

  // We must emit the PHI opcode first...
  StringRef Namespace = Target.getInstNamespace();

  if (Namespace.empty())
    PrintFatalError("No instructions defined!");

看到这里，大概可以知道指令的定义也需要 Namespace 字段，于是我们把上述 Nop 指令的定义写成::

  let Namespace = "RV32I" in {
  def Nop : Instruction {
    dag OutOperandList = (outs);
    dag InOperandList = (ins);
  }
  }

至于其他体系结构，如 RISC-V，是在 RISCVInstrFormats.td 中 ``class RVInst`` 中设置 Namespace 这个字段的。

这个时候我们再用 TableGen 的 ``--gen-instr-info`` 就能产生代码了，我们可以接着在 CMakeLists 加上::

  tablegen(LLVM RV32IGenInstrInfo.inc -gen-instr-info)

并在 ``RV32IMCTargetDesc.cpp`` 中加上::

  #define GET_INSTRINFO_MC_DESC
  #include "RV32IGenInstrInfo.inc"

  ...

  static MCInstrInfo *createRV32IMCInstrInfo() {
    MCInstrInfo *X = new MCInstrInfo();
    InitRV32IMCInstrInfo(X);
    return X;
  }

  ...
  extern "C" LLVM_EXTERNAL_VISIBILITY void LLVMInitializeRV32ITargetMC() {
    ...
    TargetRegistry::RegisterMCInstrInfo(T, createRV32IMCInstrInfo);
    ...
  }

编译时出错，提示生成的 ``RV32IGenInstrInfo.inc`` 中 MCOperandInfo 类型未定义。我们加上头文件 ``llvm/MC/MCInstrInfo.h``, 其包含的 ``llvm/MC/MCInstrDesc.h`` 包含该类。

这次再运行 ``./bin/llvm-mc --arch=rv32i --disassemble`` 并输入几个字节反汇编，错误提示变成了::

  error: unable to create instruction printer for target triple 'x86_64-unknown-linux-gnu' with assembly variant 0.

我们先不考虑为什么错误提示里的 triple 是 ``x86_64-unknown-linux-gnu``，我们接下来需要的是加上 instruction printer.
