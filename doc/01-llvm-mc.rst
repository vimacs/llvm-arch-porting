构建 llvm-mc
====================

我们的第一个目标是构建汇编器和反汇编器。LLVM 中有一个称为 ``llvm-mc`` 的程序，用于进行汇编和反汇编，我们尝试构建 ``llvm-mc``::

  $ ninja bin/llvm-mc

最后链接出错::

  /usr/bin/ld: tools/llvm-mc/CMakeFiles/llvm-mc.dir/llvm-mc.cpp.o: in function `llvm::InitializeAllTargetInfos()':
  /home/irudog/SourceCode/llvm-project/build/include/llvm/Config/Targets.def:26: undefined reference to `LLVMInitializeRV32ITargetInfo'
  /usr/bin/ld: tools/llvm-mc/CMakeFiles/llvm-mc.dir/llvm-mc.cpp.o: in function `llvm::InitializeAllTargetMCs()':
  /home/irudog/SourceCode/llvm-project/build/include/llvm/Config/Targets.def:26: undefined reference to `LLVMInitializeRV32ITargetMC'

为了能成功构建 ``llvm-mc``, 我们至少要实现 LLVMInitializeRV32ITargetInfo 和 LLVMInitializeRV32ITargetMC 函数。

对于 TargetInfo，我们模仿 RISCV，建立一个 TargetInfo 目录，添加相应的 CMake 命令，并建立一个暂时是空实现的 RV32ITargetInfo.cpp::

  #include "llvm/MC/TargetRegistry.h"

  namespace llvm {
  Target &getTheRV32ITarget() {
    static Target TheRV32ITarget;
    return TheRV32ITarget;
  }
  }

  extern "C" LLVM_EXTERNAL_VISIBILITY void LLVMInitializeRV32ITargetInfo() {
    llvm::RegisterTarget<llvm::Triple::UnknownArch> X(llvm::getTheRV32ITarget(), "rv32i",
                                      "Subset of 32-bit RISC-V", "RV32I");
  }

其中 llvm::Triple::UnknownArch 来自 include/llvm/ADT/Triple.h. 一般来说，添加一个新的体系结构，需要在这里把自己的 Triple 名字添加进去，为了方便，我们暂时在这里写 UnknownArch.

对于 TargetMC，我们也做一个空实现 MCTargetDesc/RV32IMCTargetDesc.cpp，并且添加相应的 CMake 代码::

  #include <llvm/Support/Compiler.h>

  extern "C" LLVM_EXTERNAL_VISIBILITY void LLVMInitializeRV32ITargetMC() {
  }

再跑一次 ninja, 构建成功。运行 ``bin/llvm-mc -version`` 可以看到::

  Registered Targets:
    rv32i - Subset of 32-bit RISC-V

这是因为 LLVMInitializeRV32ITargetInfo 注册了这个 target.
