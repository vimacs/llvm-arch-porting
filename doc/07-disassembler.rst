Disassembler
====================

和之前实现的东西不同，RISC-V 的 Disassembler 并没有在 RISCVMCTargetDesc.cpp 中注册，而是在 Disassembler/RISCVDisassembler.cpp 中定义和初始化。这个文件还提供了一个 C 接口::

  extern "C" LLVM_EXTERNAL_VISIBILITY void LLVMInitializeRISCVDisassembler() {
    // Register the disassembler for each target.
    TargetRegistry::RegisterMCDisassembler(getTheRISCV32Target(),
                                           createRISCVDisassembler);
    TargetRegistry::RegisterMCDisassembler(getTheRISCV64Target(),
                                           createRISCVDisassembler);
  }

这个接口来自 include/llvm-c/Target.h::

  /* Declare all of the available disassembler initialization functions. */
  #define LLVM_DISASSEMBLER(TargetName) \
    void LLVMInitialize##TargetName##Disassembler(void);
  #include "llvm/Config/Disassemblers.def"
  #undef LLVM_DISASSEMBLER  /* Explicit undef to make SWIG happier */

其中 llvm/Config/Disassemblers.def 在构建目录的 include/ 下，这个是 CMake 在配置 LLVM 的时候生成的，LLVM 源码根目录下的 CMakeLists.txt 有如下描述::

  # Produce the target definition files, which provide a way for clients to easily
  # include various classes of targets.
  configure_file(
    ${LLVM_MAIN_INCLUDE_DIR}/llvm/Config/AsmPrinters.def.in
    ${LLVM_INCLUDE_DIR}/llvm/Config/AsmPrinters.def
    )
  configure_file(
    ${LLVM_MAIN_INCLUDE_DIR}/llvm/Config/AsmParsers.def.in
    ${LLVM_INCLUDE_DIR}/llvm/Config/AsmParsers.def
    )
  configure_file(
    ${LLVM_MAIN_INCLUDE_DIR}/llvm/Config/Disassemblers.def.in
    ${LLVM_INCLUDE_DIR}/llvm/Config/Disassemblers.def
    )
  configure_file(
    ${LLVM_MAIN_INCLUDE_DIR}/llvm/Config/Targets.def.in
    ${LLVM_INCLUDE_DIR}/llvm/Config/Targets.def
    )
  
在 include/llvm/Config/Disassemblers.def.in 中可以看到 ``@LLVM_ENUM_DISASSEMBLERS@`` 这个宏，我们再回到 CMakeLists.txt::

  file(GLOB asmp_file "${td}/*AsmPrinter.cpp")
  if( asmp_file )
    set(LLVM_ENUM_ASM_PRINTERS
      "${LLVM_ENUM_ASM_PRINTERS}LLVM_ASM_PRINTER(${t})\n")
  endif()
  if( EXISTS ${td}/AsmParser/CMakeLists.txt )
    set(LLVM_ENUM_ASM_PARSERS
      "${LLVM_ENUM_ASM_PARSERS}LLVM_ASM_PARSER(${t})\n")
  endif()
  if( EXISTS ${td}/Disassembler/CMakeLists.txt )
    set(LLVM_ENUM_DISASSEMBLERS
      "${LLVM_ENUM_DISASSEMBLERS}LLVM_DISASSEMBLER(${t})\n")
  endif()

也就是说，LLVM 的构建系统是找 Target 目录下的文件，来判断这个 Target 是否有 AsmPrinter, AsmParser 和 Disassembler. 于是我们需要在 Target 目录下建一个 Disassembler 目录在其中写一个 CMakeLists.txt 文件，很自然的，Disassembler 的实现一般也在这个目录下。

在动手实现 Disassembler 之前，我们先看 RISC-V 的 Disassembler 实现。

RISCVDisassembler 是 MCDisassembler 的子类，MCDisassembler 的定义在 include/llvm/MC/MCDisassembler/MCDisassembler.h 中。主要需要实现的虚函数是 getInstruction::

  /// Returns the disassembly of a single instruction.
  ///
  /// \param Instr    - An MCInst to populate with the contents of the
  ///                   instruction.
  /// \param Size     - A value to populate with the size of the instruction, or
  ///                   the number of bytes consumed while attempting to decode
  ///                   an invalid instruction.
  /// \param Address  - The address, in the memory space of region, of the first
  ///                   byte of the instruction.
  /// \param Bytes    - A reference to the actual bytes of the instruction.
  /// \param CStream  - The stream to print comments and annotations on.
  /// \return         - MCDisassembler::Success if the instruction is valid,
  ///                   MCDisassembler::SoftFail if the instruction was
  ///                                            disassemblable but invalid,
  ///                   MCDisassembler::Fail if the instruction was invalid.
  virtual DecodeStatus getInstruction(MCInst &Instr, uint64_t &Size,
                                      ArrayRef<uint8_t> Bytes, uint64_t Address,
                                      raw_ostream &CStream) const = 0;

看接口说明和注释，大概可以知道 Disassembler 的功能是什么。再联系到之前看的 InstPrinter，大概可以知道用 LLVM 的 MC 库做反汇编操作，大致流程是先将组成指令的字符 Bytes 用 Disassembler 的 getInstruction 方法转为 MCInst 类型的 LLVM 内部指令表示，再用 InstPrinter 把 MCInst 转为人类可以读懂的汇编语言。

在 RISCVDisassembler::getInstruction 里面，我们可以看到一些 decodeInstruction 调用，如::

  Result = decodeInstruction(DecoderTable32, MI, Insn, Address, this, STI);

  Result = decodeInstruction(DecoderTableRISCV32Only_16, MI, Insn, Address,
                             this, STI);

  Result = decodeInstruction(DecoderTableRVBC16, MI, Insn, Address,
                             this, STI);

  Result = decodeInstruction(DecoderTable16, MI, Insn, Address, this, STI);

很容易想到，这是 TableGen 产生的，对 RISCV.td 运行 ``llvm-tblgen -gen-disassembler`` 可以看到 decodeInstruction 的实现，和各个以 ``DecoderTable`` 开头的 ``uint8_t`` 类型的数组。

那么这些 DecoderTable 又来自哪里？在 Target/RISCV 下尝试搜相关的字符没搜到，于是搜索 TableGen 的代码，可以看到 utils/TableGen/FixedLenDecoderEmitter.cpp 里面有这些代码::

  // Emit the decoder state machine table.
  void FixedLenDecoderEmitter::emitTable(formatted_raw_ostream &OS,
                                         DecoderTable &Table,
                                         unsigned Indentation,
                                         unsigned BitWidth,
                                         StringRef Namespace) const {
    OS.indent(Indentation) << "static const uint8_t DecoderTable" << Namespace
      << BitWidth << "[] = {\n";

就是说这些 DecoderTable 的名字是字符串 "DecoderTable" 后接上 Namespace 的名字和指令宽度。那么像上面的 ``DecoderTableRISCV32Only_16`` 这样名字的 DecoderTable, 其 Namespace 是 ``RISCV32Only_``, 搜索代码可以看到有这样的代码位于 RISCVInstrInfoC.td::

  let DecoderNamespace = "RISCV32Only_",
    Predicates = [HasStdExtC, HasStdExtF, IsRV32] in
  def C_FLW  : CLoad_ri<0b011, "c.flw", FPR32C, uimm7_lsb00>,
               Sched<[WriteFLD32, ReadMemBase]> {
    bits<7> imm;
    let Inst{12-10} = imm{5-3};
    let Inst{6} = imm{2};
    let Inst{5} = imm{6};
  }
  
可以看到 ``C_FLW`` 的确在生成的 DecoderTableRISCV32Only_16 里面。

现在我们对 RV32I 跑一次 TableGen, 可以生成 decodeInstruction 这个函数，但是没有 DecoderTable, 因为我们到现在还没有定义任何一个指令的编码，自然不可能出现译码表。

我们尝试修改以前定义的 Nop 指令。按照 LLVM 的约定，我们建立 RV32IInstrFormats.td 来定义指令格式::

  class RV32IInst<dag outs, dag ins, string opcodestr, string argstr>
      : Instruction {
    let Namespace = "RV32I";
    let Size = 4;
    field bits<32> SoftFail = 0; // we do not use this, but TableGen needs this
    field bits<32> Inst;
  
    bits<7> Opcode = 0;
    let Inst{6-0} = Opcode;
  
    dag OutOperandList = outs;
    dag InOperandList = ins;
    let AsmString = opcodestr # "\t" # argstr;
  }

在这里我们直接设定 Namespace 为 "RV32I", 这样以前的 ``let Namespace = "RV32I"`` 就不需要了，我们继承这个类就有这个 Namespace 属性。这里有两个字段是必须设的，一个是 ``Size``, 让 TableGen 知道一个指令占多少字节，一个是 ``Inst``, 让 TableGen 知道指令的编码。从 FixedLenDecoderEmitter.cpp 可以知道 TableGen 会用到这些字段。

我们重写原来只定义了一个函数的 RV32IInstrInfo.td::

  include "RV32IInstrFormats.td"

  def Nop : RV32IInst<(outs), (ins), "nop", "">;

再对 RV32I.td 允许 TableGen, 可以发现出现了一个 DecoderTable::

  static const uint8_t DecoderTable32[] = {
  /* 0 */       MCD::OPC_CheckField, 0, 7, 0, 4, 0, 0, // Skip to: 11
  /* 7 */       MCD::OPC_Decode, 240, 1, 0, // Opcode: Nop
  /* 11 */      MCD::OPC_Fail,
    0
  };

注意到在 RV32IInst 的定义中，Opcode 字段默认设为 0, 使得指令的比特 [6:0] 被设置为 0,所以 DecoderTable32 的 CheckField 检测的是从比特 0 开始的 7 比特，检测它是否为 0.

我们暂时把 TableGen 相关的内容改到这里，接下来添加 Disassembler. 建立 lib/Target/RV32I/Disassembler 目录，在其中仿照 RISCV 创建 CMakeLists.txt 和 RV32IDisassembler.cpp. 我们做一个 RV32IDisassembler.cpp 的简单实现::

  typedef MCDisassembler::DecodeStatus DecodeStatus;

  #include "RV32IGenDisassemblerTables.inc"

  DecodeStatus RV32IDisassembler::getInstruction(MCInst &MI, uint64_t &Size,
                                                 ArrayRef<uint8_t> Bytes,
                                                 uint64_t Address,
                                                 raw_ostream &CS) const {
    uint32_t Insn = support::endian::read32le(Bytes.data());
  
    DecodeStatus Result = decodeInstruction(DecoderTable32, MI, Insn, Address,
                               this, STI);
    if (Result != MCDisassembler::Fail) {
      Size = 4;
    }
    return Result;
  }

最后在 lib/Target/RV32I/CMakeLists.txt 里加上::

  tablegen(LLVM RV32IGenDisassemblerTables.inc -gen-disassembler)

  add_subdirectory(Disassembler)

最后重新编译。现在我们就有一个可用的反汇编器了::

  $ bin/llvm-mc --arch=rv32i -disassemble
  0x00 0x00 0x00 0x00
  	.text
  	nop	

  $ bin/llvm-mc --arch=rv32i -disassemble
  0x01 0x00 0x00 0x00
  	.text
  <stdin>:1:1: warning: invalid instruction encoding
  0x01 0x00 0x00 0x00
  ^

对于已经定义的指令 nop 的编码，可以反汇编出 nop 指令，而其他编码则提示无效编码。
