添加LLVM架构
===================

我准备添加RISC-V 32位架构RV32I的一个子集到LLVM中。首先在 llvm/lib/Target 下建立目录 RV32I，在里面加入 CMakeLists.txt，写上::

  add_llvm_component_group(RV32I)

然后试图用 CMake 生成构建文件::

  cmake -G Ninja -DCMAKE_BUILD_TYPE=Debug -DLLVM_TARGETS_TO_BUILD=RV32I ../llvm/

结果提示错误::

  The target `RV32I' is experimental and must be passed via
  LLVM_EXPERIMENTAL_TARGETS_TO_BUILD.

用 git grep 搜索 ``is experimental and must be passed`` 发现这个字符串在 llvm/CMakeLists.txt 里面，原因是 ``LLVM_ALL_TARGETS`` 里面没有 RV32I 这个体系结构，需要在这个文件的 ``set(LLVM_ALL_TARGETS ...)`` 这个列表里面加入 RV32I.

再运行 CMake，产生了新的错误::

  CMake Error at cmake/modules/LLVM-Config.cmake:138 (message):
    Target RV32I is not in the set of libraries.
  Call Stack (most recent call first):
    cmake/modules/LLVM-Config.cmake:256 (llvm_expand_pseudo_components)
    cmake/modules/LLVM-Config.cmake:102 (llvm_map_components_to_libnames)
    cmake/modules/LLVM-Config.cmake:95 (explicit_llvm_config)
    cmake/modules/AddLLVM.cmake:908 (llvm_config)
    cmake/modules/AddLLVM.cmake:1209 (add_llvm_executable)
    tools/llvm-gsymutil/CMakeLists.txt:11 (add_llvm_tool)

造成这个错误的是这段 CMake 语句::

  if( TARGET LLVM${c}CodeGen )
    list(APPEND expanded_components "${c}CodeGen")
  else()
    if( TARGET LLVM${c} )
      list(APPEND expanded_components "${c}")
    else()
      message(FATAL_ERROR "Target ${c} is not in the set of libraries.")
    endif()
  endif()

其中 CMake 的 TARGET 用于测试一个给定的名字是否为 CMake 的构建目标，可以参考 `CMake 的 if 命令 <https://cmake.org/cmake/help/latest/command/if.html?highlight=#existence-checks>`__.

LLVM已有的体系结构都是通过 ``add_llvm_target`` 添加一个 LLVM<arch>CodeGen 的目标。在 RV32I/CMakeLists.txt 添加::

  add_llvm_target(RV32ICodeGen
    ADD_TO_COMPONENT
    RV32I
    )

CMake 提示 ``No SOURCES given to target: LLVMRV32ICodeGen``，也就是说我要加入源码。我暂时建立一个 dummy.cpp 于 lib/Target/RV32I 下，并将以上 CMakeLists.txt 的这段改为::

  add_llvm_target(RV32ICodeGen
    dummy.cpp

    ADD_TO_COMPONENT
    RV32I
    )

再运行 CMake，这次没错误了，不过给出了大量的警告。我们会在构建我们的体系结构的组件的时候，消除这些警告。
