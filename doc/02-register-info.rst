RegisterInfo
====================

运行上一章生成的 llvm-mc 进行反汇编 ``bin/llvm-mc --arch=rv32i --disassemble`` 会产生一个断言失败::

  llvm-mc: /home/irudog/SourceCode/llvm-project/llvm/tools/llvm-mc/llvm-mc.cpp:359: int main(int, char**): Assertion `MRI && "Unable to create target register info!"' failed.

因此，我们要从 target register info 开始。

以前我曾经写程序调用过 LLVM 的反汇编库 [1]_ ，要初始化 TargetInfo, TargetMC 和 Disassembler，然后才能创建一个反汇编器实例。参考 RISCV 的 LLVMInitializeRISCVTargetMC 过程，可以发现实际上 TargetMC 的初始化包含了很多部件的初始化，其中就有 register info::

  TargetRegistry::RegisterMCRegInfo(*T, createRISCVMCRegisterInfo);

为了进一步了解 RISC-V 体系结构的代码，我们重新构建 LLVM，加上 RISCV target::

  cmake -G Ninja -DCMAKE_BUILD_TYPE=Debug -DLLVM_TARGETS_TO_BUILD='RISCV;RV32I' ../llvm/
  ninja bin/llvm-mc

用产生的 bin/llvm-mc 的 ``--version`` 选项可以看到它有 RISCV 的支持。


开始使用 TableGen
--------------------------------

首先创建一个 RV32I.td 并加入 CMakeLists.txt, 在 RV32I.td 中写上::

  include "llvm/Target/Target.td"
  include "RV32IRegisterInfo.td"

``llvm/Target/Target.td`` 中包含了大量的 LLVM 预定义的数据结构，因此基本上所有体系结构的 td 文件都会包含这个文件。一般来说，一个体系结构包含多个不同模块的定义，因此一般把这些定义分散到多个文件中，在这里我们先创建一个 ``RV32IRegisterInfo.td``.

运行 TableGen 可以看到大量的记录输出::

  bin/llvm-tblgen -I ../llvm/include/ -I ../llvm/lib/Target/RV32I/ ../llvm/lib/Target/RV32I/RV32I.td

接着我们尝试在 llvm-tblgen 后面加上 ``--gen-register-info`` 生成
RegisterInfo, TableGen 报错::

  error: ERROR: No 'Target' subclasses defined!

阅读 RISCV.td 可以发现一条 Target 子类的定义，我们搬过来::

  def RV32I : Target {
    //let InstructionSet = RISCVInstrInfo;
    //let AssemblyParsers = [RISCVAsmParser];
    //let AssemblyWriters = [RISCVAsmWriter];
    //let AllowRegisterRenaming = 1;
  }

生成 RegisterInfo
------------------------
  
运行 TableGen 可以发现生成了一条 RV32I 记录，再尝试生成 RegisterInfo::

  error: No 'RegisterClass' subclasses defined!

RegisterClass 在 ``llvm/Target/Target.td`` 中定义。我们需要先定义寄存
器，然后再通过定义 RegisterClass 的子类，指出哪些寄存器属于哪个寄存器
类::

  class RegisterClass<string namespace, list<ValueType> regTypes, int alignment,
                      dag regList, RegAltNameIndex idx = NoRegAltName>

先尝试定义两个寄存器，增加一个成为GPR的寄存器类::

  def X0: Register<"X0">;
  def X1: Register<"X1">;

  def GPR: RegisterClass<"RV32I", [i32], 32, (add (sequence "X%u", 0, 1))>;

这份简单的代码便能使 TableGen 生成 RV32IGenRegisterInfo 类。但是可以注
意到X0,X1,GPRRegClassID都不在namespace RV32I里面。所以需要设置X0,X1的
Namespace属性::

  def X0: Register<"X0"> {
      string Namespace = "RV32I";
  };
  def X1: Register<"X1"> {
      string Namespace = "RV32I";
  };

为了代码简洁，TableGen提供了如下写法::

  let Namespace = "RV32I" in {
  def X0: Register<"X0">;
  def X1: Register<"X1">;
  }

之后跑一次 llvm-tblgen -gen-register-info 生成代码，确认 X0, X1 等都在
RV32I 这个 namespace 里面。接着我们在
MCTargetDesc/RV32IMCTargetDesc.cpp 里面注册 RV32I 的 RegisterInfo::

  // We need RV32I::X0 etc.
  #define GET_REGINFO_ENUM
  #include "RV32IGenRegisterInfo.inc"

  // InitRV32IMCRegisterInfo procedure is defined in the #ifdef GET_REGINFO_MC_DESC block
  #define GET_REGINFO_MC_DESC
  #include "RV32IGenRegisterInfo.inc"

  using namespace llvm;

  static MCRegisterInfo *createRV32IMCRegisterInfo(const Triple &TT) {
    MCRegisterInfo *X = new MCRegisterInfo();
    InitRV32IMCRegisterInfo(X, /* return address */ RV32I::X1); // generated by TblGen
    return X;
  }

  extern "C" LLVM_EXTERNAL_VISIBILITY void LLVMInitializeRV32ITargetMC() {
    TargetRegistry::RegisterMCRegInfo(getTheRV32ITarget(), createRV32IMCRegisterInfo);
  }

因为 RV32IGenRegisterInfo.inc 包含多个条件编译块，所以需要用
``#define`` 使用这个文件里相应的代码块。

在 RV32I 的顶层 CMakeLists.txt 中，写上::

  set(LLVM_TARGET_DEFINITIONS RV32I.td)
  tablegen(LLVM RV32IGenRegisterInfo.inc -gen-register-info)
  add_public_tablegen_target(RV32ICommonTableGen)

接着到构建目录用 ``ninja bin/llvm-mc`` 编译，构建系统会重新调用 cmake 再编译 llvm-mc.

最后运行 llvm-mc 并调用 RV32I 的反汇编功能，这次错误信息变成::

  Assertion `MAI && "Unable to create target asm info!"' failed.

说明我们已经有了 RV32I 的 RegisterInfo.

使用预编译的 llvm-tblgen
------------------------------

之前我们构建 LLVM 的时候，都顺带构建了 llvm-tblgen，然后用它生成体系结
构需要的 inc 文件。但是当构建 Debug 版的 LLVM 的时候，构建系统编译的
llvm-tblgen 也是 Debug 版的，使得生成 inc 文件的速度变慢。所以我们有必
要先编译一个 Release 版的 llvm-tblgen，这样可以加快 inc 文件的生成速度，
而且从头编译 LLVM 的时候也无须重编译 llvm-tblgen. 此外，当需要交叉编译
LLVM 的时候，需要编译机原生的 llvm-tblgen，预编译这个程序可以使构建过
程更简单。

我们先构建一份 llvm-tblgen::

  mkdir build-tblgen
  cd build-tblgen
  cmake -G Ninja -DCMAKE_BUILD_TYPE=Release ../llvm
  ninja bin/llvm-tblgen

然后用这个 llvm-tblgen 构建 LLVM::

  mkdir build-llvm
  cd build-llvm
  cmake -G Ninja -DCMAKE_BUILD_TYPE=Debug -DLLVM_TARGETS_TO_BUILD='RISCV;RV32I' \
    -DLLVM_TABLEGEN=$(realpath ../build-tblgen/bin/llvm-tblgen) ../llvm/
  ninja bin/llvm-mc

由于这个过程还是有点麻烦，所以 LLVM 提供一个构建优化版本 TableGen 的
CMake 选项。用户可以使用 ``LLVM_OPTIMIZED_TABLEGEN=ON`` 在构建 Debug
版 LLVM 的时候，构建 Release 版的 llvm-tblgen，用它构建其他部分的 LLVM
以加快构建速度。

.. [1] https://bbs.wehack.space/thread-234.html
